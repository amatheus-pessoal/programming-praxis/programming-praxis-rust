//!
//! Potholes
//! May 11, 2021
//! Today’s exercise comes from one of those online code exercises, via Stack Overflow:
//!
//! There is a machine that can fix all potholes along a road 3 units in length. A unit of road will be represented by a period in a string. For example, “…” is one section of road 3 units in length. Potholes are marked with an “X” in the road, and also count as 1 unit of length. The task is to take a road of length N and fix all potholes with the fewest possible sections repaired by the machine.
//!
//! The city where I live needs one of those machines.
//!
//! Here are some examples:
//!
//! ```text
//! .X.          1
//! .X...X       2
//! XXX.XXXX     3
//! .X.XX.XX.X   3
//! ```
//! Your task is to write a program that takes a string representing a road and
//! returns the smallest number of fixes that will remove all the potholes. When
//! you are finished, you are welcome to read or run a suggested solution, or to
//! post your own solution or discuss the exercise in the comments below.
pub mod pothole_fixer {
    pub fn fix(road_repr: &str) -> usize {
        let chars: Vec<char> = road_repr.chars().collect();
        solve(&chars, 0)
    }

    fn solve(chars: &[char], idx: usize) -> usize {
        if chars.len() <= 3 {
            if chars.contains(&'X') {
                1
            } else {
                0
            }
        } else {
            match chars[0] {
                '.' => solve(&chars[1..], idx + 1),
                'X' => {
                    let s1 = if idx >= 2 {
                        Some(solve(&chars[1..], idx + 1))
                    } else {
                        None
                    };
                    let s2 = if idx >= 1 {
                        Some(solve(&chars[2..], idx + 2))
                    } else {
                        None
                    };
                    let s3 = Some(solve(&chars[3..], idx + 3));

                    let mut valid: Vec<usize> =
                        vec![s1, s2, s3].into_iter().filter_map(|o| o).collect();

                    valid.sort();

                    valid.get(0).cloned().unwrap_or(0) + 1
                }
                c => panic!("Invalid char in input: {}", c),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::pothole_fixer::*;

    #[test]
    fn test_example1_from_site() {
        assert_eq!(fix(".X"), 1);
    }

    #[test]
    fn test_example2_from_site() {
        assert_eq!(fix(".X...X"), 2);
    }

    #[test]
    fn test_example3_from_site() {
        assert_eq!(fix("XXX.XXXX"), 3);
    }

    #[test]
    fn test_example4_from_site() {
        assert_eq!(fix(".X.XX.XX.X"), 3);
    }

    #[test]
    fn test_edge_case() {
        assert_eq!(fix("..XX.."), 1);
    }
}
