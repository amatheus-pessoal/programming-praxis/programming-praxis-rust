use std::error::Error;
use rpncalculator::RpnCalculator;

fn main() {
    let mut calculator = RpnCalculator::default();
    calculator.push(19);
    calculator.push(2.14);
    calculator.op("+");
    calculator.push(4.5);
    calculator.push(2);
    calculator.push(4.3);
    calculator.op("/");
    calculator.op("-");
    calculator.op("*");
    if let Some(result) = calculator.top() {
        println!("Result of [19 2.14 + 4.5 2 4.3 / - *]: {}", result);
    } else {
        println!("Error: No result returned.");
    }
}