//!
//! # Problem description:
//! #
//! # RPN Calculator
//! *February 19, 2009*
//!
//! Implement an RPN calculator that takes an expression like 19 2.14 +
//! 4.5 2 4.3 / - * which is usually expressed as (19 + 2.14) * (4.5 - 2 /
//! 4.3) and responds with 85.2974. The program should read expressions
//! from standard input and print the top of the stack to standard output
//! when a newline is encountered. The program should retain the state of
//! the operand stack between expressions.
//!
use std::{ops, error, fmt};
use std::collections::HashMap;
use std::fmt::{Display, Formatter};

pub struct RpnCalculator {
    operations: HashMap<String, Operation>,
    stack: Vec<f64>,
}

#[derive(Debug)]
pub enum Error {
    NotEnoughOperands,
    InvalidOperation { op: String },
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {}

#[derive(Clone)]
enum Operation {
    Unary(fn(f64) -> f64),
    Binary(fn(f64, f64) -> f64),
    Poly { number_operands: usize, operation: fn(Vec<f64>) -> f64 },
}

impl Operation {
    fn operate(&self, calculator: &mut RpnCalculator) -> Result<(), Error> {
        match self {
            Operation::Unary(op) => {
                let operand = calculator.pop_operand()?;
                let result = op(operand);
                calculator.push(result);
            }
            Operation::Binary(op) => {
                let op2 = calculator.pop_operand()?;
                let op1 = calculator.pop_operand()?;
                let result = op(op1, op2);
                calculator.push(result);
            }
            Operation::Poly { number_operands, operation } => {
                let mut operands = Vec::new();
                for _i in 0..*number_operands {
                    let operand = calculator.pop_operand()?;
                    operands.push(operand);
                }
                operands.reverse();
                let result = operation(operands);
                calculator.push(result);
            }
        }
        Ok(())
    }
}

impl Default for RpnCalculator {
    fn default() -> Self {
        let stack = Vec::new();
        let operations: HashMap<String, Operation> = vec![
            (String::from("+"), Operation::Binary(ops::Add::add)),
            (String::from("-"), Operation::Binary(ops::Sub::sub)),
            (String::from("*"), Operation::Binary(ops::Mul::mul)),
            (String::from("/"), Operation::Binary(ops::Div::div)),
        ].into_iter().collect();
        RpnCalculator { stack, operations }
    }
}

impl RpnCalculator {
    pub fn push(&mut self, operand: impl Into<f64>) {
        self.stack.push(operand.into());
    }

    pub fn top(&self) -> Option<&f64> {
        self.stack.last()
    }

    pub fn stack_depth(&self) -> usize {
        self.stack.len()
    }

    pub fn op(&mut self, op: &str) -> Result<(), Error> {
        if let Some(operation) = self.operations.get(op).cloned() {
            operation.operate(self)
        } else {
            Err(Error::InvalidOperation { op: String::from(op) })
        }
    }

    fn pop_operand(&mut self) -> Result<f64, Error> {
        self.stack.pop().ok_or(Error::NotEnoughOperands)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_top_should_be_empty_after_creating_calculator() {
        let calculator = RpnCalculator::default();
        assert!(calculator.top().is_none());
        assert_eq!(calculator.stack_depth(), 0);
    }

    #[test]
    fn test_after_pushing_top_should_show_pushed_value() {
        let mut calculator = RpnCalculator::default();
        calculator.push(10);
        assert_eq!(*calculator.top().unwrap(), 10.0);
        assert_eq!(calculator.stack_depth(), 1);
    }

    #[test]
    fn test_applies_binary_operation_to_the_stack() {
        let mut calculator = RpnCalculator::default();
        calculator.push(1);
        calculator.push(1);
        calculator.op("+").unwrap();
        assert_eq!(*calculator.top().unwrap(), 2.0);
        assert_eq!(calculator.stack_depth(), 1);
    }
}